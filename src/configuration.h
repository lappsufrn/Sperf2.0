//
// Created by vitor on 02/03/18.
//

#ifndef SPERF2_0_CONFIGURATION_H
#define SPERF2_0_CONFIGURATION_H

#include <fstream>
#include <vector>
#include <string>
#include <regex>

#include "util.h"

class configuration
{
protected:
    std::vector<ulong> list_threads, frequency;
    std::vector<std::string> list_args;
    ulong number_tests= 0;
public:
    void load_from_file(const std::string& file_name);
    void create_default(const std::string& file_name);
};


#endif //SPERF2_0_CONFIGURATION_H
