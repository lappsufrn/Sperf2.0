
#pragma once

#include <fstream>
#include <vector>
#include <map>
#include <set>

#include "configuration.h"
class output : public virtual configuration
{
    enum agregation
    {
        none,
        max,
        sum
    };
protected:
    std::string program_name, config_file, output_name;
    std::map<std::tuple<int,int,int,std::string>,std::vector<s_info>> data; // mark, n_exec, thr, arg -> vector<s_info>
    std::set<int> marks;
    agregation agg= max;
    void save_json_core(int m, std::ofstream& save);
public:
    bool check_format(std::string& format);
    void set_agg(std::string method);
    void save(std::string format);
    s_info get_max(std::vector<s_info> data);
    void save_xml();
    void save_csv();
    void save_json();
    void save_json_one_file();
};