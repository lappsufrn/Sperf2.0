//
// Created by vitor on 02/03/18.
//

#include "util.h"

using namespace std;

vector<string> split(const string& s, char delimiter)
{
    vector<string> tokens;
    string token;
    istringstream tokenStream(s);
    while(getline(tokenStream, token, delimiter))
    {
        token.erase(remove(token.begin(), token.end(), '\n'), token.end());
        //token = regex_replace(token, regex(" +"), "$1");
        if(!token.empty())
            tokens.push_back(token);
    }
    return tokens;
}

char** convert(const vector<string>& v)
{
    char** args= new char*[v.size()+1];
    for(unsigned int i=0; i<v.size(); i++)
        args[i]= const_cast<char*>(v[i].c_str());
    args[v.size()]= nullptr;
    return args;
}

void create_folder(string path)
{
    if(opendir(path.c_str()) == nullptr)
    {
        cout << GREEN "[Sperf]" RESET " Creating folder "+path << endl;
        if(mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)<0)
            throw  "Failed to create folder: \n";
    }
}

double time_diff(const s_info s)
{
    return s.s_stop_time-s.s_start_time;
}