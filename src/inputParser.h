//
// Created by vitor on 02/03/18.
//

#ifndef SPERF2_0_INPUTPARSER_H
#define SPERF2_0_INPUTPARSER_H

#include <vector>
#include <string>
#include <map>

class inputParser
{
    std::map<std::string,std::string> options;
    std::vector<std::string> words;
    std::string program_path;
public:
    inputParser(int argc, char** argv);
    std::string get_program_path();
    const std::string getOption(const std::string& opt);
    const bool hasOption(const std::string& opt); 
    std::vector<std::string> get_words();
};


#endif //SPERF2_0_INPUTPARSER_H
