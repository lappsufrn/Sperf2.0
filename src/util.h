//
// Created by vitor on 02/03/18.
//

#ifndef SPERF2_0_UTIL_H
#define SPERF2_0_UTIL_H

#include <iostream>
#include <string>
#include <string>
#include <vector>
#include <regex>


#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/time.h>

#define RED     			"\x1b[31m"
#define GREEN   			"\x1b[32m"
#define YELLOW  		"\x1b[33m"
#define BLUE    			"\x1b[34m"
#define MAGENTA 		"\x1b[35m"
#define CYAN   			"\x1b[36m"
#define RESET   			"\x1b[0m"

#define GET_TIME(now) { \
   struct timeval t; \
   gettimeofday(&t, NULL); \
   now = t.tv_sec + t.tv_usec/1000000.0; \
}

struct s_info {
    int s_mark;
    double s_start_time, s_stop_time;
    int s_start_line;
    int s_stop_line;
    char s_filename[64];
};

double time_diff(const s_info s);

std::vector<std::string> split(const std::string& s, char delimiter);
char** convert(const std::vector<std::string>& v);
void create_folder(std::string path);


#endif //SPERF2_0_UTIL_H
