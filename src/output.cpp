
#include <iostream>
#include <fstream>
#include "output.h"
#include "util.h"

using namespace std;

bool output::check_format(string& format)
{
    format= format.empty()?"json1":format;
    if(format != "csv" && format != "json1" && format != "json2" 
    && format != "xml" && format != "all")
    {
        cerr << RED "[SPERF]" RESET <<  " Unsupported output type " << format << " changing to json" << endl;
        format= "json1";
        return false;
    }
    return true;
}

void output::set_agg(string method)
{
    if(method == "max")
        agg= agregation::max;
    else if(method == "none")
        agg= agregation::none;
    else
        cerr << RED "[SPERF]" RESET <<  " Unsupported aggregation type " << method << " changing to max" << endl;

}

void output::save(string format)
{
    if(format == "csv")
        save_csv();
    else if(format == "json1")
        save_json_one_file();
    else if(format == "json2")
        save_json();
    else if(format == "xml")
        save_xml();
    else if(format == "all")
    {
        save_csv();
        save_xml();
        save_json();
        save_json_one_file();
    }
    else
        cerr << RED "[SPERF]" RESET <<  " Unsupported output type " << format << endl;

}

s_info output::get_max(vector<s_info> data)
{
    s_info max={};
    if(data.empty())
        return max;
    max= data[0];
    for(const auto& x : data)
    {
        if(time_diff(x)>time_diff(max))
            max= x;
    }
    return max;
}

void output::save_xml()
{
    if(output_name.find('/') != string::npos)
        create_folder(output_name.substr(0,output_name.find_last_of('/')));
}

void output::save_csv()
{
    if(output_name.find('/') != string::npos)
        create_folder(output_name.substr(0,output_name.find_last_of('/')));
    cout << GREEN "[Sperf]" RESET " Saving to the file" << endl;
    ofstream save;
    save.exceptions(ofstream::failbit | ofstream::badbit);
    for(const auto& m : marks)
    {
        int m_=m;
        if(m>0) m_--; else if(m<0) m_++;
        try
        {
            if(m_ != m)
                save.open(output_name+"region_"+to_string(m_)+".csv");
            else
                save.open(output_name+".csv");
        }
        catch(const ofstream::failure& error)
        {
            cerr << RED "[SPERF]" RESET " Error saving the file " << output_name << " for region " << m_ << endl;
            cerr << RED "[SPERF]" RESET " Continuing to next region" << endl;
            continue;
        }
        save << ",";
        for(const auto& arg : list_args)
            save << arg << ",";
        save << endl << endl;
        for(ulong i=0; i<number_tests; i++)
        {
            for(const auto& thr : list_threads)
            {
                save << thr << ",";
                for(const auto& arg : list_args)
                {
                    double serial_time= time_diff( get_max(data[make_tuple(m,i,1,arg)]) );
                    double parallel_time= time_diff( get_max(data[make_tuple(m,i,thr,arg)]) );
                    save << serial_time/parallel_time << ",";
                }
                save << "\n";
            }
            save << "\n";
        }
        save.close();
    }
}

void output::save_json_core(int m, ofstream& save)
{
    for(ulong i=0; i<number_tests; i++)
    {
        save << "\t\t[\n";
        for(const auto& arg : list_args)
        {
            save << "\t\t\t{\n"
                    << "\t\t\t\t\"argument\":\"" << arg << "\",\n"
                    << "\t\t\t\t\"runs\":[\n";
            for(const auto& thr : list_threads)
            {
                double serial_time= time_diff( get_max(data[make_tuple(m,i,1,arg)]) );
                double parallel_time= time_diff( get_max(data[make_tuple(m,i,thr,arg)]) );
                double ini_time= data[make_tuple(0,i,thr,arg)][0].s_start_time;

                save << "\t\t\t\t\t{\n"
                        << "\t\t\t\t\t\t\"threads\":" << thr << ",\n"
                        << "\t\t\t\t\t\t\"enters\":\n";
                if(agg == agregation::max)
                    save << "\t\t\t\t\t\t{\n"
                            << "\t\t\t\t\t\t\t\t\"time\":" << parallel_time << ",\n"
                            << "\t\t\t\t\t\t\t\t\"speedup\":" << serial_time/parallel_time << "\n"
                            << "\t\t\t\t\t\t}\n";
                else if(agg == agregation::none)
                {
                    save << "\t\t\t\t\t\t[\n";
                    for(const auto& x : data[make_tuple(m,i,thr,arg)])
                    {
                        save << "\t\t\t\t\t\t\t{\n"
                            << "\t\t\t\t\t\t\t\t\"start_time\":" << x.s_start_time-ini_time << ",\n"
                            << "\t\t\t\t\t\t\t\t\"stop_time\":" << x.s_stop_time-ini_time << ",\n"
                            << "\t\t\t\t\t\t\t\t\"speedup\":" << serial_time/time_diff(x) << "\n"
                            << "\t\t\t\t\t\t\t}";
                        if( &x == &data[make_tuple(m,i,thr,arg)].back() )
                            save << "\n";
                        else
                            save << ",\n";
                    }
                    save << "\t\t\t\t\t\t]\n";
                }
                save << "\t\t\t\t\t}";
                if(thr != list_threads.back())
                    save << ",\n";
                else
                    save << "\n";
            }
            save << "\t\t\t\t]\n"
                    << "\t\t\t}";
            if(&arg != &list_args.back())
                save << ",\n";
            else
                save << "\n";
        }
        save << "\t\t]";
        if(i != number_tests-1)
            save << ",\n";
        else
            save << "\n";
    }
}

void output::save_json()
{
    if(output_name.find('/') != string::npos)
        create_folder(output_name.substr(0,output_name.find_last_of('/')));
    cout << GREEN "[Sperf]" RESET " Saving to the file" << endl;
    ofstream save;
    save.exceptions(ofstream::failbit | ofstream::badbit);
    for(const auto& m : marks)
    {
        int m_=m;
        if(m>0) m_--; else if(m<0) m_++;
        try
        {
            if(m_ != m)
                save.open(output_name+"region_"+to_string(m_)+".json");
            else
                save.open(output_name+".json");
        }
        catch(const ofstream::failure& error)
        {
            cerr << RED "[SPERF]" RESET " Error saving the file " << output_name << " for region " << m_ << endl;
            cerr << RED "[SPERF]" RESET " Continuing to next region" << endl;
            continue;
        }
        auto current_file= data[make_tuple(m,0,1,list_args[0])];
        save << "{\n"
                << "\t\"start_line\" : \"" << current_file[0].s_start_line << "\",\n"
                << "\t\"stop_line\"  : \"" << current_file[0].s_stop_line  << "\",\n"
                << "\t\"filename\" : \"" << current_file[0].s_filename << "\",\n"
                << "\t\"executions\":\n\t[\n";
        
        save_json_core(m, save);

        save << "\t]\n"
             << "}";
        save.close();
    }
}

void output::save_json_one_file()
{
    if(output_name.find('/') != string::npos)
        create_folder(output_name.substr(0,output_name.find_last_of('/')));
    cout << GREEN "[Sperf]" RESET " Saving to the file"  << endl;
    ofstream save;
    save.exceptions(ofstream::failbit | ofstream::badbit);
    try
    {
        save.open(output_name+".json");
    }
    catch(const ofstream::failure& error)
    {
        cerr << RED "[SPERF]" RESET " Error saving the file " << output_name << endl;
        cerr << RED "[SPERF]" RESET " Continuing to next region" << endl;
        return;
    }
    save << "[\n";
    for(const auto& m : marks)
    {
        int m_=m;
        if(m>0) m_--; else if(m<0) m_++;
        auto current_file= data[make_tuple(m,0,1,list_args[0])];
        save << "{\n"
                << "\t\"region\" : \"" << current_file[0].s_start_line << ", "
                                        << current_file[0].s_stop_line  << "\",\n"
                << "\t\"filename\" : \"" << current_file[0].s_filename << "\",\n"
                << "\t\"executions\":\n\t[\n";
        
        save_json_core(m, save);

        save << "\t]\n"
             << "}";
        if(m != *(--marks.end()))
            save << ",\n";
        else
            save << "\n";
    }
    save << "]\n";
    save.close();
}