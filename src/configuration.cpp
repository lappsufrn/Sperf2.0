//
// Created by vitor on 02/03/18.
//

#include "configuration.h"

using namespace std;

void configuration::load_from_file(const string& file_name)
{
    ifstream ifs;
    smatch m_value;

    cout << GREEN "[SPERF]" RESET " Reading " << file_name << endl;

    ifs.exceptions(ifstream::failbit | ifstream::badbit);
    try
    {
        ifs.open(file_name);
    }
    catch(const ifstream::failure& error)
    {
        cerr << RED "[SPERF]" RESET " Error opening configuration file " << file_name << endl;
        cerr << GREEN "[SPERF]" RESET " Trying to create default configuration file " << file_name << endl;
        create_default(file_name);

        ifs.open(file_name); // check error to
    }

    string file((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));

    if(regex_search(file, m_value, regex("number_of_tests *= *([0-9]+)")))
        number_tests= stoul(m_value[1]);
    else
        throw "Missing number_of_tests";

    if(regex_search(file, m_value, regex("list_of_args *=[\n, ]*\\{([\\s\\S]+)\\}")))
        list_args= split(m_value[1], ',');

    if(regex_search(file, m_value, regex("list_of_threads *= *\\{ *(([0-9]+ *[,]{1}? *)*[0-9]+) *\\}")))
    {
        vector<string> res= split(m_value[1], ',');
        transform(res.begin(), res.end(), back_inserter(list_threads), [](const string& str) { return stoul(str); });
    }
    else
    {
        string type_of_step;
        ulong step, max_threads;

        if(regex_search(file, m_value, regex("type_of_step *= *(power|constant)")))
            type_of_step= m_value[1];
        else
            throw "Missing type_of_step";

        if(regex_search(file, m_value, regex("value_of_step *= *([1-9][0-9]*)")))
            step= stoul(m_value[1]);
        else
            throw "Missing value_of_step";

        if(regex_search(file, m_value, regex("max_number_threads *= *([0-9]+)")))
            max_threads= stoul(m_value[1]);
        else
            throw "Missing max_number_threads";

        list_threads.push_back(1);
        if(type_of_step == "constant")
        {
            for(ulong k=1+step; k<=max_threads; k+=step)
                list_threads.push_back(k);
        }
        else if(type_of_step == "power")
        {
            for(ulong k=step; k<=max_threads; k*=step)
                list_threads.push_back(k);
        }
    }
}

void configuration::create_default(const std::string &file_name) {
    //create_folder("../etc/");
    ofstream save;
    save.exceptions(ofstream::failbit | ofstream::badbit);
    try
    {
        save.open(file_name);
    }
    catch(const ofstream::failure& error)
    {
        throw "Cannot create the configuration file";
    }
    save << "#\n"
         << "# Configuration file of Sperf execution.\n"
         << "#\n"
         << "# Number of tests on the target application.\n\n"
         << "number_of_tests = 1\n\n"
         << "# List of number of threads to be executed. \n"
         << "# The numbers must be comma-separeted\n"
         << "# The execution with 1 thread is mandatory and must not be specified.\n\n"
         << "list_of_threads = {}\n\n"
         << "# Maximum number of threads to be executed\n"
         << "# Used only if the list_threads_values is not defined\n\n"
         << "max_number_threads = 8\n\n"
         << "# Step of execution. \"type_of_step\" may be the values \"constant\" and \"power\". \n"
         << "#\"value_of_step\" is the step value. \n"
         << "# If \"constant\" is defined, it will occurr the adittion of the \"value_of_step\" on the number of threads.\n"
         << "# Else, a \"value_of_step\" power will be applied.\n"
         << "# Used only if the list_threads_values is not defined\n\n"
         << "type_of_step = power\n"
         << "value_of_step = 2\n\n"
         << "# list of argument separated by comma\n\n"
         << "list_of_args={}\n";
    save.close();
    cout << GREEN "[SPERF]" RESET " Default configuration create with sucess" << endl;
}
