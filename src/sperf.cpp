#include <iostream>

#include "util.h"
#include "inputParser.h"
#include "instrumentation.h"
#include "profiler.h"

using namespace std;

int main(int argc, char** argv)
{
    try
    {
        inputParser parser(argc, argv);
        string instrument_path= parser.getOption("-i");
        string config_file= parser.getOption("-c");
        string output_type= parser.getOption("-t");
        string output_file= parser.getOption("-o");
        string agg_method= parser.getOption("-g");
        vector<string> pargs= parser.get_words();
        bool help= parser.hasOption("--h") || pargs.empty();
        bool preload= parser.hasOption("--b");

        if(!instrument_path.empty())
        {
            string type_of_inst= parser.getOption("-e");
            vector<string> extesions= split(type_of_inst,',');
            try
            {
                instrumenter mark(parser.get_program_path(),instrument_path, extesions);
                mark.instrument();
            }
            catch(const char* error)
            {
                cerr << RED "[SPERF]" RESET << " " << error << endl;
            }
        }
        else if(help)
        {
            cerr << GREEN "[SPERF] " RESET "Usage on instrumented binary ./sperf program" << endl;
            cerr << GREEN "[SPERF] " RESET "Options:" << endl;
            cerr << GREEN "[SPERF] " RESET "--h help" << endl;
            cerr << GREEN "[SPERF] " RESET "-c configuration file name" << endl;
            cerr << GREEN "[SPERF] " RESET "-o output file name" << endl;
            cerr << GREEN "[SPERF] " RESET "-t output file type (json1, json2, csv, all)" << endl;
            cerr << GREEN "[SPERF] " RESET "-g agregation method (max none) default max" << endl;
            cerr << GREEN "[SPERF] " RESET "Source code Instrumentation (openmp)" << endl;
            cerr << GREEN "[SPERF] " RESET "-i path to the source code" << endl;
            cerr << GREEN "[SPERF] " RESET "-e list of extensions separated by comma" << endl;
            cerr << GREEN "[SPERF] " RESET "Usage on non instrumented bynary (openmp) ./sperf --b program" << endl;
        }
        else
        {
            profiler sperf(parser.get_program_path(), pargs,config_file, output_file, preload);
            sperf.check_format(output_type);
            if(agg_method != "") sperf.set_agg(agg_method);
            sperf.run();
            sperf.save(output_type);
        }
    }
    catch (const string& error)
    {
        cerr << RED "[SPERF]" RESET << " " << error << endl;
    }
    catch (const char* error)
    {
        cerr << RED "[SPERF]" RESET << " " << error << endl;
    }
    catch (const ifstream::failure& error)
    {
        cerr << RED "[SPERF]" RESET " Exception opening/reading/closing file " << error.what() << " " << error.code() << endl;
    }
}