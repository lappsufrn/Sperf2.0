//
// Created by vitor on 27/02/18.
//

#include "instrumentation.h"

using namespace std;

instrumenter::instrumenter(const string& program_path_, const string& instru_path_, const vector<string>& extensions_)
{
    program_path= program_path_;
    instru_path= instru_path_;
    extensions= extensions_;

    if(instru_path.empty())
        throw "Error instrumenter empty directory";

    if(extensions.empty())
    {
        cout << GREEN "[Sperf]" RESET " Using default extensions .c .cpp" << endl;
        extensions.emplace_back(".cpp");
        extensions.emplace_back(".c");
    }
    
    if(instru_path == ".") instru_path= "./";
    
    if(instru_path.back() == '/')
    {
        cout << GREEN "[Sperf]" RESET " Retriving file names" << endl;
        get_files_dpath();
    }
    else
    {
        cout << GREEN "[Sperf]" RESET " file " << instru_path << endl;
        files.push_back(instru_path.substr(instru_path.find_last_of('/')+1));
        instru_path= instru_path.substr(0,instru_path.find_last_of('/')+1);
    }

}

void instrumenter::get_files_dpath()
{
    DIR *dp;
    dirent *dptr;
    dp = opendir(instru_path.c_str());
    if(dp == nullptr)
        throw "Cannot open the directory "+instru_path;
    while((dptr= readdir(dp)) != nullptr)
    {
        string dir= dptr->d_name;
        for(const auto& ext: extensions)
        {
            size_t idx= dir.find_last_of('.');
            if(idx != string::npos && dir.substr(idx)==ext)
            {
                cout << GREEN "[Sperf]" RESET  << " Found file " << dir << endl;
                files.push_back(dir);
                break;
            }
        }
    }
}

size_t instrumenter::find_omp_directives(const string& str, size_t pos)
{
    string tofind= "#pragmaompparallel";
    for(size_t l=pos; l<str.size(); l++)
    {
        size_t r=l, cont_= 0;
        while(r<str.size() && cont_ < tofind.size() && str[r] == tofind[cont_++])
        {
            r++;
            while(r<str.size() && str[r] == ' ')
                r++;
        }
        if(cont_ == tofind.size())
            return l;
    }
    return string::npos;
}

void instrumenter::find_comment_regions(const string& txt, const string& e1, const string& e2, vector<commentRegion> &commentRegions)
{
    commentRegion cR= {0, 0};
    size_t p= txt.find(e1);
    while(p!=string::npos)
    {
        cR.li= p;
        cR.lf= txt.find(e2,p+e2.size());
        commentRegions.push_back(cR);
        p= txt.find(e1, cR.lf);
    }
}

bool instrumenter::is_insid_comment(size_t pos, const vector<commentRegion> &commentRegions)
{
    for(const auto& crs: commentRegions)
        if(pos>crs.li && pos<crs.lf)
            return true;
    return false;
}

void instrumenter::instrument()
{
    create_folder(instru_path+"instr/");
    cout << GREEN "[Sperf]" RESET " Parsing the files..." << endl;
    ifstream ifs;
    ofstream save;
    ifs.exceptions(ifstream::failbit | ifstream::badbit);
    save.exceptions(ofstream::failbit | ofstream::badbit);
    for(const auto& file_name: files)
    {
        string file_content;
        try
        {
            ifs.open(instru_path+file_name);
            file_content.assign((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));
        }
        catch(...)
        {
            throw "Error opening "+instru_path+file_name+" to instrument";
        }

        vector<commentRegion> commentRegions;
        size_t pos;
        string mark_start, mark_stop;
        bool have_omp= false;
        int id_region= 0;

        auto forward_to= [](const string& str, size_t& pos, char s1, char s2)
        {
            int cont= 0;
            do
            {
                if(str[pos]==s1)
                    cont++;
                if(str[pos]==s2)
                    cont--;
                pos++;
            }while(pos<str.size() && cont>0);
        };

        pos= find_omp_directives(file_content, 0);
        while(pos != string::npos)
        {
            commentRegions.clear();
            find_comment_regions(file_content, "//", "\n", commentRegions);
            find_comment_regions(file_content, "/*", "*/", commentRegions);
            if(!is_insid_comment(pos, commentRegions))
            {
                have_omp= true;
                cout << GREEN "[Sperf]" RESET " Found parallel regions on " << file_name << endl;

                mark_start= "\nsperf_start("+to_string(id_region)+");\n";
                file_content= file_content.substr(0, pos)+mark_start+file_content.substr(pos);

                commentRegions.clear();
                find_comment_regions(file_content, "//", "\n", commentRegions);
                find_comment_regions(file_content, "/*", "*/", commentRegions);

                size_t stp= pos+mark_start.size();
                do
                {
                    while(is_insid_comment(stp, commentRegions))
                        stp++;
                    if(file_content[stp]=='(')
                    {
                        forward_to(file_content, stp, '(', ')');
                        continue;
                    }
                    if(file_content[stp] == '{')
                    {
                        forward_to(file_content, stp, '{', '}');
                        break;
                    }
                    if(file_content[stp]==';')
                    {
                        stp++;
                        break;
                    }
                    stp++;
                }while(stp<file_content.size());
                mark_stop= "\nsperf_stop("+to_string(id_region)+");\n";
                file_content= file_content.substr(0, stp)+mark_stop+file_content.substr(stp);
                long int l1= count(file_content.begin(),file_content.begin()+pos,'\n')-id_region*4;
                long int l2= l1+count(file_content.begin()+pos,file_content.begin()+stp,'\n');
                cout << GREEN "[Sperf]" RESET " Marks on lines " << l1 << " " << l2 << endl;
                pos= find_omp_directives(file_content, pos+mark_start.size()+1);
                id_region++;
            }
            else
                pos= find_omp_directives(file_content, pos+1);
        }
        if(have_omp)
        {
            ifs.close();
            try
            {
                if(getenv("SPERF_DIR"))
                {
                    string pp= getenv("SPERF_DIR");
                    ifs.open(pp+"/include/sperfops.h");
                }
                else ifs.open(program_path+"../include/sperfops.h");
            }
            catch(const ifstream::failure& error)
            {
                throw "Cannot open sperfops.h ";
            }
            string sperfops_h((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));
            sperfops_h+=file_content;

            save.open(instru_path+"instr/"+file_name);
            save << sperfops_h;
            save.close();
        }
        ifs.close();
    }
}