//
// Created by vitor on 02/03/18.
//

#include <sys/wait.h>

#include "profiler.h"
#include "util.h"

using namespace std;

profiler::profiler(string prof_path_, vector<string> program_args, 
                   string config_file_, string output_name_, bool preload_)
{
    preload= preload_;
    prof_path= prof_path_.empty()?".":prof_path_;
    if(program_args.empty())
        throw "No program passed to sperf";

    program_name= program_args[0];
    if(program_name[0] != '/' && program_name[0] != '.')
        program_name="./"+program_name;
    if(config_file_.empty())
        config_file= prof_path+program_name.substr(program_name.find_last_of('/'))+".conf";
    else
        config_file= config_file_;

    if(output_name_.empty())
        output_name= prof_path+program_name.substr(program_name.find_last_of('/'))+"_out_";
    else
        output_name= output_name_;

    string parg;
    for(unsigned int i=1; i<program_args.size(); i++)
        parg+=program_args[i]+" ";

    if(!parg.empty())
        list_args.push_back(parg);
    load_from_file(config_file);
    if(list_args.empty())
        list_args.push_back("");
}

void profiler::run()
{
    cout << GREEN "[SPERF]" RESET " Copyright (C) 2017" << endl;
    for(ulong num_exec=0; num_exec<number_tests; num_exec++)
    {
        cout << GREEN "[SPERF]" RESET " Current execution " << num_exec+1 << " of " << number_tests << endl;
        for(const auto& thr : list_threads)
        {
            for(const auto& arg : list_args)
                execute(num_exec,thr,arg);
        }
    }
}

void profiler::execute(ulong num_exec, ulong thr, string arg)
{
    if(pipe(fd_pipe)<0)
        throw "Erro creating pipe";
    pid_t pid= fork();
    if(pid < 0)
        throw "Erro on fork";
    else if(pid == 0)
    {
        if(close(fd_pipe[0])<0)
            throw "Erro closing pipe";

        string current_arg= program_name+" "+regex_replace(arg, regex("__nt__"), to_string(thr));

        vector<string> aux= split(current_arg,' ');
        char **args= convert(aux);

        string buff= "FD_PIPE="+to_string(fd_pipe[1]);
        string nthreads= "OMP_NUM_THREADS="+to_string(thr);
        string ld_library_path= "LD_LIBRARY_PATH=";
        string ld_preload= "LD_PRELOAD=";
        if(getenv("LD_LIBRARY_PATH"))
            ld_library_path+=string(getenv("LD_LIBRARY_PATH"));
        if(getenv("LD_PRELOAD"))
            ld_preload+=string(getenv("LD_PRELOAD"))+" ";
        if(preload)
            ld_preload+="libgomp_isnt.so";
        char *envp[] = {&buff[0], &nthreads[0], &ld_preload[0], &ld_library_path[0], 0};
        cout << GREEN "[SPERF]" RESET " Executing " << thr  << " threads, arg : " << current_arg << endl;

        if(execvpe(program_name.c_str(), args, envp) < 0)
            throw "Erro executing program";
        delete []args;
    }
    else
    {
        if(close(fd_pipe[1])<0)
            throw "Erro closing pipe";

        struct timeval t1, t2;
        gettimeofday(&t1, NULL);
        while(!waitpid(pid, nullptr, WNOHANG)) // wait the program finish
        {
            s_info info= {};
            if(read(fd_pipe[0], &info, sizeof(s_info)) == sizeof(s_info))
            {
                string current_arg= program_name+" "+regex_replace(arg, regex("__nt__"), to_string(thr));
                int m_= info.s_mark>=0?info.s_mark+1:info.s_mark-1;
                data[make_tuple(m_,num_exec,thr,arg)].push_back(info);
                marks.insert(m_);
            }
        }
        gettimeofday(&t2, NULL);
        s_info info= {0,t1.tv_sec + t1.tv_usec/1000000.0,t2.tv_sec + t2.tv_usec/1000000.0,0,0};
        marks.insert(0);
        //string current_arg= program_name+" "+regex_replace(arg, regex("__nt__"), to_string(thr));
        data[make_tuple(0,num_exec,thr,arg)].push_back(info);
    }
}