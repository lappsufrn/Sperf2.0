//
// Created by vitor on 27/02/18.
//

#ifndef SPERF2_0_INSTRUMENTATION_H
#define SPERF2_0_INSTRUMENTATION_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "util.h"

class instrumenter
{
    std::string program_path, instru_path;
    std::vector<std::string> extensions, files;
private:
    struct commentRegion
    {
        size_t li, lf;
    };
    size_t find_omp_directives(const std::string& txt, size_t p);
    void find_comment_regions(const std::string& txt, const std::string &e1, const std::string &e2, std::vector<commentRegion> &commentRegions);
    bool is_insid_comment(size_t pos, const std::vector<commentRegion> &commentRegions);
    void get_files_dpath();
public:
    instrumenter(const std::string& program_path_, const std::string& instru_path_, const std::vector<std::string>& extensions_);
    void instrument();
};


#endif //SPERF2_0_INSTRUMENTATION_H
