//
// Created by vitor on 02/03/18.
//

#include "inputParser.h"

using namespace std;

inputParser::inputParser(int argc, char** argv)
{
    program_path= argv[0];
    for(int i=1; i<argc; i++)
    {
        if(argv[i][0]=='-' && argv[i][1]!='-')
        {
            if(i+1 < argc)
                options.emplace(argv[i], argv[i+1]);
            i++;
            continue;
        }
        else if(argv[i][1]=='-')
        {
            options.emplace(argv[i], "");
        }
        else
            words.emplace_back(argv[i]);
    }
}
string inputParser::get_program_path()
{
    return program_path.substr(0,program_path.find_last_of('/')+1);
}
const bool inputParser::hasOption(const string& opt)
{
    auto it= options.find(opt);
    if(it != options.end())
        return true;
    return false;
}
const string inputParser::getOption(const string& opt)
{
    auto it= options.find(opt);
    if(it != options.end())
        return it->second;
    return "";
}
vector<string> inputParser::get_words()
{
    return words;
}