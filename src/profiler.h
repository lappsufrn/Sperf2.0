//
// Created by vitor on 02/03/18.
//

#ifndef SPERF2_0_PROFILER_H
#define SPERF2_0_PROFILER_H

#include "configuration.h"
#include "output.h"
class profiler : public output, public virtual configuration
{
    int fd_pipe[2];
    std::string prof_path;
    bool preload= false;
private:
    void execute(ulong num_exec, ulong thr, std::string arg);
public:
    profiler(std::string prof_path_,std::vector<std::string> program_args, 
             std::string config_file_, std::string output_name_, bool preload_);
    void run();
};


#endif //SPERF2_0_PROFILER_H
