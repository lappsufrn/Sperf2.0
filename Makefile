CODE_DIR = src/
LIB_DIR = include/
TEST_DIR = tests/

.PHONY: all

all:
	$(MAKE) -C $(CODE_DIR)
	mkdir -p bin
	mv src/sperf bin/
	$(MAKE) -C $(LIB_DIR)

test:
	$(MAKE) -C $(TEST_DIR)

install:
	cp include/sperfops.h /usr/include
	cp include/libgomp_isnt.so /usr/lib
	cp bin/sperf /usr/bin/

clean:
	rm bin/sperf
	rm src/*.o
	rm include/libgomp_isnt.so
