DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
echo $DIR
export SPERF_DIR=$DIR
export PATH=$PATH:$DIR/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DIR/include