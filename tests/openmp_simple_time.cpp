#include <iostream>
#include <omp.h>
#include <sperfops.h>
#include <unistd.h>

using namespace std;

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Invalid number of arguments" << endl;
        return -1;
    }
    int t= stof(argv[1]);

    sperf_start(0);
    #pragma omp parallel
    {
        usleep(t*omp_get_num_threads()*1E4);
    }
    sperf_stop(0);

    sperf_start(1);
    #pragma omp parallel
    {
        usleep(t*2E4/omp_get_num_threads()+1E4);
    }
    sperf_stop(1);


    sperf_start(-1);
    #pragma omp parallel
    {
        usleep(t*3E4/omp_get_num_threads());
    }
    sperf_stop(-1);

    return 0;
}