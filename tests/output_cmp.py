import json, os
import matplotlib.pyplot as plt
import numpy as np
import numbers

def extract(data):
    y= []
    if isinstance(data, list):
        for x in data:
            y+=extract(x)
    elif isinstance(data, dict):
        for x in data:
            y+=extract(x)
            y+=extract(data[x])
    else:
        y.append(data)
    return y

def rel_cmp(a, b):
    if a==b:
        return True
    if a/abs(a-b) > 0.1:
        return True
    return False

def comp_list(l1, l2):
    structure= True
    values= True
    if len(l1) != len(l2):
        return False, False
    for x1,x2 in zip(l1,l2):
        if isinstance(x1, numbers.Number) and isinstance(x2, numbers.Number):
            if not rel_cmp(x1,x2):
                values= False
        elif isinstance(x1, str) and isinstance(x2, str):
            if x1 != x2:
                values= False
        else:
            structure= False
    return structure, values

total= 0
nfiles= 0
if __name__ == '__main__':
    for f in os.listdir('../bin/output'):
        d1= json.loads(open('../bin/output/'+f).read())
        d2= json.loads(open('standard_files/'+f).read())

        l1= extract(d1)
        l2= extract(d2)

        total+=sum(comp_list(l1,l2))
        nfiles+=2
        print(f, comp_list(l1,l2))

print('TOTAL {}%'.format(int(total/nfiles*100)))
        

'''
begin= []
end= []
event= []
for r in data[1:2]:
    # print('Region {} {}'.format(*r['region'].split(', ')))
    # print(r['filename'])
    for i,e in enumerate(r['executions']):
        # print('Execution {}'.format(i))
        for arg in e[-1:]:
            # print('Argument {}'.format(arg['argument']))
            for thr in arg['runs'][-1:]:
                print('Threads {}'.format(thr['threads']))
                # print('start_time {}'.format(thr['start_time']))
                # print('stop_time {}'.format(thr['stop_time']))
                # print('speedup {}'.format(thr['speedup']))
                for j, loops in enumerate(thr['enters']):
                    begin.append(loops['start_time'])
                    end.append(loops['stop_time'])
                    event.append('R {} {} - {}'.format(*r['region'].split(', ')+[j]))
        break

begin= np.asarray(begin)
end= np.asarray(end)
print(begin)
print(end)

# event = ["Event {}".format(i) for i in range(len(begin))]
plt.barh(range(len(begin)),  end-begin, left=begin)
plt.yticks(range(len(begin)), event)
plt.show()
'''