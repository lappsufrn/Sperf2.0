# test openmp_simple_time
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Enter $(pwd)"
cd $DIR/../

echo "Making"
#make clean
make
source env.sh
echo "Making test"
make test

echo "Enter bin"
cd bin

echo "Testing json output"
sperf openmp_simple_time 10 -t json1 -o output/f1 # json test
sperf openmp_multiple_time 10 -t json1 -o output/f2 # json test
sperf --b openmp_simple_bin 10 -t json1 -o output/f3 # json test

echo "Testing csv output"
sperf openmp_simple_time 10 -t csv -o output/f1 # json test
sperf openmp_multiple_time 10 -t csv -o output/f2 # json test
sperf --b openmp_simple_bin 10 -t csv -o output/f3 # json test

echo "TOTAL 100%"