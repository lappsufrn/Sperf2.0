#include <iostream>
#include <omp.h>
#include <sperfops.h>
#include <unistd.h>

using namespace std;

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Invalid number of arguments" << endl;
        return -1;
    }
    int t= stof(argv[1]);

    #pragma omp parallel
    {
        sperf_start(0);
        usleep(t*(1+omp_get_thread_num())*1E4);
        sperf_stop(0);
    }

    #pragma omp parallel
    {
        sperf_start(1);
        usleep(t*(1+omp_get_thread_num())*2E4);
        sperf_stop(1);
    }

    #pragma omp parallel
    {
        sperf_start(-1);
        usleep(t*(1+omp_get_thread_num())*3E4);
        sperf_stop(-1);
    }

    return 0;
}