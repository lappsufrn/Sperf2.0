#define _GNU_SOURCE

#include <dlfcn.h>

#include <sys/time.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define RED     			"\x1b[31m"
#define RESET   			"\x1b[0m"

#define GET_TIME(now) { \
   struct timeval t; \
   gettimeofday(&t, NULL); \
   now = t.tv_sec + t.tv_usec/1000000.0; \
}

#define	 MAX_ANNOTATIONS	64
#define  MAX_THREADS        128

struct s_info {
    int s_mark;
    double s_start_time, s_stop_time;
    int s_start_line;
    int s_stop_line;
    char s_filename[64];
};

int fd_pipe;
unsigned char flag_conf = 0;

static void setconfig()
{
    if(!flag_conf)
    {
        char *str_pipe;

        str_pipe = getenv("FD_PIPE");
        if(!str_pipe)
        {
            fprintf(stderr, RED "[Sperf]" RESET " Sperf not running\n");
            exit(1);
        }
        fd_pipe = atoi(str_pipe);

        flag_conf= 1;
    }
}

static void fname(char * last, const char * f)
{
	const char * delim = "/";
	char * token, * s;
	int i = 0;

	s = strdup(f);
	token = strtok(s, delim);
	while( token != NULL )
   	{
		i++;
		strcpy(last, token);
		token = strtok(NULL, delim);
  	 }
}

#define sperf_start(id) _sperf_start(id, __LINE__, __FILE__);
#define sperf_stop(id) _sperf_stop(id, __LINE__, __FILE__);
void _sperf_start(int id, int start_line, const char * filename);
void _sperf_stop(int id, int stop_line, const char * filename);

static double id_time[MAX_ANNOTATIONS][MAX_THREADS];
static double id_start_line[MAX_ANNOTATIONS][MAX_THREADS];


void _sperf_start(int id, int start_line, const char * filename)
{
    setconfig();
    static double now;
    GET_TIME(now);
    id_time[id<0?(MAX_ANNOTATIONS+id):id][omp_get_thread_num()%MAX_THREADS]= now;
    id_start_line[id<0?(MAX_ANNOTATIONS+id):id][omp_get_thread_num()%MAX_THREADS]= start_line;
}

void _sperf_stop(int id, int stop_line, const char * filename)
{
    static double time_final;
    GET_TIME(time_final);
    //time_final-=id_time[id<0?(MAX_ANNOTATIONS+id):id][omp_get_thread_num()%MAX_THREADS];

    struct s_info info;

    info.s_mark = id;
    info.s_start_time = id_time[id<0?(MAX_ANNOTATIONS+id):id][omp_get_thread_num()%MAX_THREADS];
    info.s_stop_time = time_final;
    info.s_start_line = id_start_line[id<0?(MAX_ANNOTATIONS+id):id][omp_get_thread_num()%MAX_THREADS];
    info.s_stop_line = stop_line;

    char only_filename[64];
    fname(only_filename, filename);
    strcpy(info.s_filename, only_filename);

    if (!flag_conf || (int) write(fd_pipe, &info, sizeof(struct s_info)) == -1)
    {
        fprintf(stderr, RED "[Sperf]" RESET " Writing to the pipe has failed: %s\n", strerror(errno));
        exit(1);
    }
}

/*
#pragma omp parallel
{
    body;
} 

becomes

void subfunction (void *data)
{
    use data;
    body;
}

setup data;
GOMP_parallel_start (subfunction, &data, num_threads);
subfunction (&data);
GOMP_parallel_end ();
*/

static int cont_regions= 0;

void(*body_data)(void*);

void instrumented_fn(void *data)
{
    _sperf_start(cont_regions,0,"binary");
    body_data(data);
    _sperf_stop(cont_regions,0,"binary");
}
// overwrite GOMP_parallel from libgomp.so and instrument
void GOMP_parallel(void (*fn) (void *), void *data, unsigned num_threads)
{
    void (*original_GOMP_parallel)(void (*) (void *), void *, unsigned);
    original_GOMP_parallel = dlsym(RTLD_NEXT, "GOMP_parallel");
    body_data= fn;
    cont_regions++;
    original_GOMP_parallel(instrumented_fn, data, num_threads);
}